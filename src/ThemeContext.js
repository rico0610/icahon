//Context API

// import React from 'react';

// const UserContext = React.createContext();

// export const UserProvider = UserContext.Provider;
// export default UserContext;

import { createContext } from "react";

export const themes = {
  dark: "",
  light: "white-content",
};

export const ThemeContext = createContext({
    theme: themes.dark,
    changeTheme: () => {},
});